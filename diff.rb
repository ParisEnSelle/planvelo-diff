require 'bundler/setup'
require 'geojson-diff'

after = File.open(ARGV[1]).read
before = File.open(ARGV[0]).read
diff = GeojsonDiff.new(before,after)

# File.open("diff.geojson", 'w') { |file| file.write(diff.diff().to_json) }

["added", "removed", "unchanged"].each do |type|
  file = "_data/#{type}.geojson"
  File.open(file, 'w') { |file| file.write(diff.send(type).to_json) }
  puts "#{file} diff created."
end
